<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'App\Http\Controllers\WebController@homeCheckout')->name('checkout');

Route::get('/test/{itemid}', '\App\Http\Controllers\ApiController@getItem')->name('get-item');
Route::get('/test2/{itemid}', '\App\Http\Controllers\ApiController@calculatePrices');