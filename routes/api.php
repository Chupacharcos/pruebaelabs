<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('get-item/{itemId}', '\App\Http\Controllers\ApiController@getItem')->name('get-item');
Route::post('calculate-prices', '\App\Http\Controllers\ApiController@calculatePrices')->name('calculate-prices');
