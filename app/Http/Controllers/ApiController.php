<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Item;

class ApiController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param string $itemId
     * @return mixed
     */
    public function getItem(Request $request, $itemId) {

        $entrada=Item::find($itemId);
        return $entrada;
    }

     /**
     * CREATE THE METHOD calculatePrices RETURNING AN ARRAY WITH THE FINAL PRICE AND THE COSTS OF THE TAXES (21%)
     */
     
    public function calculatePrices(Request $request, $itemId) {
        $entrada=Item::selectRaw('price as Price, price * 0.21 as Taxes, price * 1.21 as Total')->get();
        return $entrada;
    }
   

}
