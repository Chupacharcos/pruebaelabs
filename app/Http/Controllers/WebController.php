<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Item;

class WebController extends Controller
{
    function homeCheckout(Request $request) {
        $tplVars['item'] = Item::first();
        $tplVars['item']->image = asset($tplVars['item']->image);
        return view('checkout', $tplVars);
    }
}
